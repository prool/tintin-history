This package was debianized by Jordi Mallach <jordi@debian.org> on
Wed, 22 Aug 2001 03:23:36 +0200.

It was downloaded from http://mail.newclear.net/tintin/archive.htm.

Upstream Authors: Davin Chan <davinchan@earthlink.net>
		  Robert Ellsworth <rse@newclear.net>
	See the CREDITS document for the full list of contributors.


Copyright:

Tintin++ was relicensed under the GNU General Public License on
July 12th, 2001, when version 1.86 was released. The full text
of the GNU GPL can be found in /usr/share/common-licenses/GPL on
Debian systems.

Additionally, the Tintin++ manual was placed under the GPL at my request
by Robert Ellsworth, it's author. The following is the mail in which he
granted me permission to publish it under the new license:

> From: "Robert Ellsworth" <rellsworth@wattsup.com>
> Subject: RE: Tintin install mess
> To: "'Davin Chan'" <davinchan@earthlink.com>,
>         "'Jordi Mallach'" <jordi@sindominio.net>
> Date: Tue, 28 Aug 2001 09:53:50 -0400
> In-Reply-To: <3B8AF5A5.557E35E0@earthlink.com>
> X-Mailer: Microsoft Outlook, Build 10.0.2627
>
> Well,
>
> RE #1) Well, being that I am the author, then yes, I have no problem
> with the changing of the manual to GPL. :-)
